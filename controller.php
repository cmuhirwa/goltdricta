<?php
if ($_SERVER["REQUEST_METHOD"] == "POST" || $_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$request = array_merge($_POST, $_GET);
		if(isset($request['action']))
		{
			$action = $request['action'];

			//check if the function exists
			if(function_exists($action)){
				//call the function
				$action();
			}else{
				echo 'Invalid request';
			}
		}else
		{
			echo 'Please read the API documentation';
		}
	}
	else
	{
		echo 'UPLUS API V02';
	}

// CONTACTS
function createContact()
{
	$contactId			= rand(1000,9999);
	$contactName		= $_POST['contactName'];
	$contactCompany		= $_POST['contactCompany'];
	$contactStreet		= $_POST['contactStreet'];
	$contactCity		= $_POST['contactCity'];
	$contactSector		= $_POST['contactSector'];
	$contactProvince	= $_POST['contactProvince'];
	$contactCountry		= $_POST['contactCountry'];
	$contactVoice 		= $_POST['contactVoice'];
	$contactEmail		= $_POST['contactEmail'];
	$contactPassword	= $_POST['contactPassword'];

	include 'db.php';
	// CHECK IF THE ACCOUNT EXIST
	$sqlCheckDuplicates = $db->query("SELECT * FROM contacts WHERE contactEmail = '$contactEmail'");
	if(mysqli_num_rows($sqlCheckDuplicates) == 0)
	{
		echo $query = $db->query("INSERT INTO contacts (contactId, contactName, contactCompany, contactStreet, contactCity, contactSector, contactProvince, contactCountry, contactVoice, contactEmail, contactPassword, createdBy) VALUES ('$contactId', '$contactName', '$contactCompany', '$contactStreet', '$contactCity', '$contactSector', '$contactProvince', '$contactCountry', '$contactVoice', '$contactEmail', '$contactPassword', 1)")or die(mysqi_error($db));
	}
	else
	{
		echo '1';
	}
}

function login()
{
	$contactEmail		= $_POST['contactEmail'];
	$contactPassword	= $_POST['contactPassword'];

	include 'db.php';
	// CHECK IF THE AUTHENTICATION PASSES
	$sqlCheckDuplicates = $db->query("SELECT * FROM contacts WHERE contactEmail = '$contactEmail' AND contactPassword = '$contactPassword'");
	if(mysqli_num_rows($sqlCheckDuplicates) > 0)
	{
		session_start();
		$_SESSION['contactEmail']  		= $contactEmail;
		$_SESSION['contactPassword']  	= $contactPassword;
		echo "1";
	}
	else
	{
		echo "Worng Email or Password";
	}
}
function listContact()
{
	include 'db.php';
	//contactName contactCompany contactStreet contactCity contactSector
	//contactProvince contactCountry contactVoice contactEmail contactPassword
}

// DOMAINS
function createDomain()
{
	include 'db.php';
	$domainName			= $_POST['domainName'];
	$domainPeriod		= $_POST['domainPeriod'];
	$domainHost1		= $_POST['domainHost1'];
	$domainHost2		= $_POST['domainHost2'];
	$domainRegistrant	= $_POST['domainRegistrant'];
	$domainAdmin		= $_POST['domainAdmin'];
	$domainTech			= $_POST['domainTech'];
	$domainPassword		= $_POST['domainPassword'];
	$phoneNumber		= $_POST['phoneNumber'];
	$amount				= 100; //$_POST['amount'];
	$status 			= 'CALLED';
	$time 				= time();
	$hash 				= hash('sha256', ($time.''.$phoneNumber.''.$amount));
	session_start();
	unset($_SESSION['holddomian']);
	$_SESSION['holddomian'] = "";

	// CHECK IF THE DOMAIN EXIST
	$sqlCheckDuplicates = $db->query("SELECT * FROM domains WHERE domainName = '$domainName'");
	if(mysqli_num_rows($sqlCheckDuplicates) == 0)
	{
		$query = $db->query("INSERT INTO 
			domains (domainName, domainPeriod, domainHost1, domainHost2, domainRegistrant, domainAdmin, domainTech, domainPassword, createdBy, status, hash) 
			VALUES ('$domainName', '$domainPeriod', '$domainHost1', '$domainHost2', '$domainRegistrant', '$domainAdmin', '$domainTech', '$domainPassword', '1', '$status', '$hash')")or die(mysqi_error($db));
		
		$status = mtnpay($amount, $phoneNumber, $hash, $time);
		if($status == 'FAILED'){
			echo 'You dont have enough funds to pay this domain'.$_SESSION['holddomian'];
		}
		else
		{
			echo 'Please Confirm your transaction on your mobile money number'.$_SESSION['holddomian'];
		}

		$query = $db->query("UPDATE domains SET status = '$status' WHERE hash = '$hash'")or die(mysqi_error($db));
	}
	else
	{

		//$status = mtnpay($amount, $phoneNumber, $hash, $time);
		$status = 'PENDING';
		if($status == 'FAILED'){
			echo 'You dont have enough funds to pay this domain'.$_SESSION['holddomian'];
		}
		else
		{
			echo 'Please Confirm your transaction on your mobile money number';
		}

		$query = $db->query("UPDATE domains SET status = '$status' WHERE domainName = '$domainName'")or die(mysqi_error($db));
	}
}

function listDomain()
{
	include 'db.php';
}

// HOSTS
function createHost()
{
	//hostName hostIpv41 hostIpv42 hostIpv6
	include 'db.php';
}

function listHost()
{
	include 'db.php';
}

function mtnpay($amount, $phone, $hash, $time)
{
	$curl = curl_init();
	

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "http://uplus.rw/bridge/",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "action=deposit&amount=$amount&phonenumber=$phone&clienttime=$time&appToken=8ff84623a67c8e9d0e15&hash=$hash",
	  CURLOPT_HTTPHEADER => array(
	    "Content-Type: application/x-www-form-urlencoded"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  return json_decode($response)->status;
	}
}
?>