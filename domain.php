<!DOCTYPE html>
<html>
<head>
	<title>RICTA API</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    
</head>
<body>
<div class="container">
	<br/>
	<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm"></div>
		<div class="col-sm">
			<a href="login.php"  class="btn btn-success btn-sm">Login</a> or 
			<a href="signup.php"  class="btn btn-warning btn-sm">Signup</a>
		</div>
		</hr>
	</div>
	<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm">
			<input type="text" id="domainsearch" placeholder="Enter A Domain to buy"/>
			<button onclick="domainsearch()" class="btn btn-dark btn-sm">Search</button>
			<hr><br>
			<div id="resultsHolder"></div>
		</div>
		<div class="col-sm"></div>
		</hr>
	</div>
</div>
<script type="text/javascript" src="jquery341.js"></script>
<script type="text/javascript">
	function domainsearch() {
		var domainname	= document.getElementById('domainsearch').value;

		$.ajax({
				type: 'GET',
				url : "client/domainCheck.php",
				dataType : "html",
				cache : "false",
				data : {
					domainname 			: domainname
				},
				success : function(html, textStatus){
					results = JSON.parse(html);
					domainreason = 'domainreason';
					if(results.response.resData.domainchkData.domaincd.hasOwnProperty(domainreason))
					{
						$("#resultsHolder").html(results.response.resData.domainchkData.domaincd.domainreason);
					}
					else
					{
						$.ajax({
							type: 'GET',
							url : "client/domainCheck.php",
							dataType : "html",
							cache : "false",
							data : {
								holddomian		: domainname
							},
							success : function(html, textStatus){
								$("#resultsHolder").html("The domain is available click here to buy it <a  class='btn btn-success btn-sm' href='login.php'>Buy Domain</a>");
							}
						});
					}
				},
				error : function(xht, textStatus, errorThrown){
					alert('Error: '+ errorThrown); 
				}
			});
	}
</script>
</body>
</html>