<?php 
	session_start();
	$contactEmail		= $_SESSION['contactEmail'];
	$contactPassword	= $_SESSION['contactPassword'];

	include 'db.php';
	// CHECK IF THE AUTHENTICATION PASSES
	$sqlCheckDuplicates = $db->query("SELECT * FROM contacts WHERE contactEmail = '$contactEmail' AND contactPassword = '$contactPassword'");
	if(mysqli_num_rows($sqlCheckDuplicates) > 0)
	{
		while ($contactRow = mysqli_fetch_array($sqlCheckDuplicates))
		{
			$contactId 		= $contactRow['contactId'];
			$contactName 	= $contactRow['contactName'];
		}
		echo 'Welcome '.$contactName;
	}
	else
	{
		header("LOCATION: logout.php");
	}?>

<!DOCTYPE html>
<html>
<head>
	<title>RICTA API</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">   
</head>
<body>
<div class="container">
	<a href="logout.php" class="btn btn-danger btn-sm">Logout</a>
	<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm">
			<h4>Domains</h4>
			<h6>Create a Domain</h6>
				<?php 
					if (empty($_SESSION['holddomian']))
					{
						echo '<input type="text" class="form-control form-control-sm" id="domainName" placeholder="Please logout first" disabled>';
					}
					else {
						echo '<input type="text" class="form-control form-control-sm" id="domainName" value="'.$_SESSION['holddomian'].'" disabled>';
					}					
				?>
				
				<input type="number" class="form-control form-control-sm" id="domainPeriod" placeholder="Domain Period">
				<input type="text" class="form-control form-control-sm" id="domainHost1" placeholder="Domain Host1">
				<input type="text" class="form-control form-control-sm" id="domainHost2" placeholder="Domain Host2">
				<input type="text" class="form-control form-control-sm" id="domainRegistrant" hidden value="<?php echo $contactId; ?>">
				<br>Authentication<br>
				<input type="text" class="form-control form-control-sm" id="domainAdmin" placeholder="Domain Admin">
				<input type="text" class="form-control form-control-sm" id="domainTech" placeholder="Domain Tech">
				<input type="password" class="form-control form-control-sm" id="domainPassword" placeholder="Domain Password">
				<br>Payment<br>
				<input type="text" class="form-control form-control-sm" value="12,500Rwf" disabled>
				<input type="number" id="amount" value="12500" hidden>
				<input type="text" class="form-control form-control-sm" id="phoneNumber" placeholder="MTN Phone Number">
				<br>
				<button onclick="createDomain()" class="btn btn-success btn-sm">Buy</button>
				<hr>
			<h6>Your Domains</h6>
			<ul>
				<?php 
					$listDomainsQuery = $db->query("SELECT * FROM domains");
					while ($rowDomains = mysqli_fetch_array($listDomainsQuery)) 
					{
						$domainName = $rowDomains['domainName'];
						$domainStatus = $rowDomains['status'];
						echo '<li><b>'.$domainName.'</b> | (Doamin status: Purchase '.$domainStatus.')';
						if ($domainStatus == 'SUCCESSFUL') {
							echo '<button class="btn btn-danger btn-sm">Transfer</button></li>';
						}
						else
						{
							echo '</li>';
						}
					}
				?>	
			</ul>
		</div>
		<div class="col-sm"></div>
	</div>
</div>
<script type="text/javascript" src="jquery341.js"></script>
<script type="text/javascript">
	function createDomain() {
		var domainName		= document.getElementById('domainName').value;
		var domainPeriod	= document.getElementById('domainPeriod').value;
		var domainHost1		= document.getElementById('domainHost1').value;
		var domainHost2		= document.getElementById('domainHost2').value;
		var domainRegistrant= document.getElementById('domainRegistrant').value;
		var domainAdmin		= document.getElementById('domainAdmin').value;
		var domainTech		= document.getElementById('domainTech').value;
		var domainPassword	= document.getElementById('domainPassword').value;	
		var phoneNumber		= document.getElementById('phoneNumber').value;	
		var amount			= document.getElementById('amount').value;	

		$.ajax({
				type: 'POST',
				url : "controller.php",
				dataType : "html",
				cache : "false",
				data : {
					action 				: 'createDomain',
					domainName			: domainName,
					domainPeriod		: domainPeriod,
					domainHost1			: domainHost1,
					domainHost2			: domainHost2,
					domainRegistrant	: domainRegistrant,
					domainAdmin			: domainAdmin,
					domainTech			: domainTech,
					domainPassword		: domainPassword,
					phoneNumber			: phoneNumber,
					amount				: amount
				},
				success : function(html, textStatus){
					if(html == '1')
					{
						alert(html)
						//alert('Domain added successfully.');
						//window.location.href = "index.php";
					}
					else
					{
						alert(html);
					}
				},
				error : function(xht, textStatus, errorThrown){
					alert('Error: '+ errorThrown); 
				}
			});				
	}
	function createHost() {
		var hostName		= document.getElementById('hostName').value;
		var hostIpv41		= document.getElementById('hostIpv41').value;
		var hostIpv42		= document.getElementById('hostIpv42').value;
		var hostIpv6		= document.getElementById('hostIpv6').value;						
	}
</script>
</body>
</html>

