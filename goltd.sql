-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2019 at 12:08 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `goltd`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `contactId` varchar(50) NOT NULL,
  `contactName` varchar(50) NOT NULL,
  `contactCompany` varchar(50) NOT NULL,
  `contactStreet` varchar(50) NOT NULL,
  `contactCity` varchar(50) NOT NULL,
  `contactSector` varchar(50) NOT NULL,
  `contactProvince` varchar(50) NOT NULL,
  `contactCountry` varchar(50) NOT NULL,
  `contactVoice` varchar(50) NOT NULL,
  `contactEmail` varchar(50) NOT NULL,
  `contactPassword` varchar(50) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` datetime DEFAULT NULL,
  `archive` enum('NO','YES') NOT NULL DEFAULT 'NO',
  `archivedBy` int(11) DEFAULT NULL,
  `archivedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `contactId`, `contactName`, `contactCompany`, `contactStreet`, `contactCity`, `contactSector`, `contactProvince`, `contactCountry`, `contactVoice`, `contactEmail`, `contactPassword`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`, `archive`, `archivedBy`, `archivedDate`) VALUES
(1, '8037', 'test', 'testco', 'khtest', 'testcity', 'testsector', 'testprovince', 'testcountry', '0859879834', 'muhirwaclement@gmail.com', 'clement123', 1, '2019-12-04 10:28:26', NULL, NULL, 'NO', NULL, NULL),
(2, '9085', 'Test1', 'test1', 'khtests', 'kigali', 'fdskj', 'fsdkj', 'Rwanda', '0809879834', 'test1@gmail.com', 'test123', 1, '2019-12-18 07:29:06', NULL, NULL, 'NO', NULL, NULL),
(3, '4822', 'Test2', 'Test2', 'khtest', 'testcity', 'testsector', 'testprovince', 'Rwanda', '078579834', 'test2@gmail.com', 'test123', 1, '2019-12-18 07:32:05', NULL, NULL, 'NO', NULL, NULL),
(4, '4908', 'Test4', 'TEst5', 'Tekdn', 'kdjvnk', 'dkjn', 'kjdnk', 'kdjn', '039470789', 'test4', '123', 1, '2019-12-27 11:58:02', NULL, NULL, 'NO', NULL, NULL),
(5, '4089', 'Test4', 'TEst5', 'Tekdn', 'kdjvnk', 'dkjn', 'kjdnk', 'kdjn', '039470789', 'test4@gmail.com', '123', 1, '2019-12-27 11:58:27', NULL, NULL, 'NO', NULL, NULL),
(6, '6316', 'Test5', 'Test5Comp', 'Kg41Ave', 'Kigali', 'Kimironko', 'Kigali', 'Rwanda', '0784848236', 'muhirwaclement1@gmail.com', 'clement123', 1, '2019-12-27 11:59:20', NULL, NULL, 'NO', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `domains`
--

CREATE TABLE `domains` (
  `id` int(11) NOT NULL,
  `domainName` varchar(50) NOT NULL,
  `domainPeriod` int(11) NOT NULL,
  `domainHost1` varchar(50) NOT NULL,
  `domainHost2` varchar(50) NOT NULL,
  `domainRegistrant` varchar(50) NOT NULL,
  `domainAdmin` varchar(50) NOT NULL,
  `domainTech` varchar(50) NOT NULL,
  `domainPassword` varchar(50) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` datetime DEFAULT NULL,
  `archive` enum('NO','YES') NOT NULL DEFAULT 'NO',
  `archivedBy` int(11) DEFAULT NULL,
  `archivedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `domains`
--

INSERT INTO `domains` (`id`, `domainName`, `domainPeriod`, `domainHost1`, `domainHost2`, `domainRegistrant`, `domainAdmin`, `domainTech`, `domainPassword`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`, `archive`, `archivedBy`, `archivedDate`) VALUES
(1, '', 0, '', '', '4822', '', 'muhirwaclement@gmail.com', 'clement123', 1, '2019-12-18 12:16:32', NULL, NULL, 'NO', NULL, NULL),
(2, 'testdomain2', 12, 'testhost1', 'testhost2', '4822', 'clement', 'muhirwaclement@gmail.com', 'clement123', 1, '2019-12-27 11:29:12', NULL, NULL, 'NO', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hosts`
--

CREATE TABLE `hosts` (
  `id` int(11) NOT NULL,
  `hostName` varchar(50) NOT NULL,
  `hostIpv41` varchar(50) NOT NULL,
  `hostIpv42` varchar(50) NOT NULL,
  `hostIpv6` varchar(50) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` datetime NOT NULL,
  `archive` enum('NO','YES') NOT NULL DEFAULT 'NO',
  `archivedBy` int(11) NOT NULL,
  `archivedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domains`
--
ALTER TABLE `domains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hosts`
--
ALTER TABLE `hosts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `domains`
--
ALTER TABLE `domains`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hosts`
--
ALTER TABLE `hosts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
