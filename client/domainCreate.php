<?php
	if (isset($_POST['domainName'])) {
	
	echo $domainName			= $_POST['domainName'];
		$domainPeriod		= $_POST['domainPeriod'];
		$domainHost1		= $_POST['domainHost1'];
		$domainHost2		= $_POST['domainHost2'];
		$domainRegistrant	= $_POST['domainRegistrant'];
		$domainAdmin		= $_POST['domainAdmin'];
		$domainTech			= $_POST['domainTech'];
		$domainPassword		= $_POST['domainPassword'];
		//$phoneNumber		= $_POST['phoneNumber'];
		//echo ".".$domainRegistrant.".";
	}
	else
	{
		echo "nothing is set";
	}
//Login Parameters
$certpath=dirname(__FILE__) .'/test.pem';
$params=array("Username" => "go_rwxtn","Password"=>"XTAB3JwZ4LwGcR4H","Server"=>"registry2.ricta.org.rw","Port"=>"700","Certificate"=>$certpath,"SSL"=>"on");
try {
$client = epp_Client($params);
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}

//Domain Check
$request =  $client->request($xml ='<?xml version="1.0" encoding="UTF-8" standalone="no"?>
   	<epp xmlns="urn:ietf:params:xml:ns:epp-1.0">
     	<command>
       		<create>
	         	<domain:create xmlns:domain="urn:ietf:params:xml:ns:domain-1.0">
	           	<domain:name>'.$domainName.'</domain:name>
	           	<domain:period unit="y">'.$domainPeriod.'</domain:period>
	           	<domain:ns>
	             	<domain:hostObj>'.$domainHost1.'</domain:hostObj>
	             	<domain:hostObj>'.$domainHost2.'</domain:hostObj>
	           	</domain:ns>
	           	<domain:registrant>'.$domainRegistrant.'</domain:registrant>
	           	<domain:contact type="admin">'.$domainRegistrant.'</domain:contact>
	           	<domain:contact type="tech">'.$domainRegistrant.'</domain:contact>
	           	<domain:authInfo>
	             	<domain:pw>'.$domainPassword.'</domain:pw>
	           		</domain:authInfo>
	         	</domain:create>
       		</create>
	       	<clTRID>'.rand(10000, 99999).'</clTRID>
     	</command>
   	</epp>
');

                    

                   # Parse XML result
	$doc= new DOMDocument();
	$doc->loadXML($request);
       

	$coderes = $doc->getElementsByTagName('result')->item(0)->getAttribute('code');
	$msg = $doc->getElementsByTagName('msg')->item(0)->nodeValue;

	# Check results
                    if($coderes != '1000') {
			
		echo "Code (".$coderes.") ".$msg;
                    }
 print_r($doc->saveXML());
 #logs
file_put_contents(dirname(__FILE__) .'/debug/domain-check-aroc-out.xml', $doc->saveXML(), FILE_APPEND);
file_put_contents(dirname(__FILE__) .'/debug/domain-check-in-aroc.xml', $xml, FILE_APPEND);


function epp_Client($params) {
	# Setup include dir
	
	
	# Include EPP stuff we need
	require_once dirname(__FILE__) . '/Net/EPP/Client.php';
	require_once dirname(__FILE__) .'/Net/EPP/Protocol.php';


	# Grab module parameters
	

	# Are we using ssl?
	$use_ssl = false;
	if (isset($params['SSL']) && $params['SSL'] == 'on') {
		$use_ssl = true;
	}

	# Set certificate if we have one
 	if ($use_ssl && !empty($params['Certificate'])) {
		if (!file_exists($params['Certificate'])) {
			return PEAR_Error("Certificate file does not exist");
		}

		# Create SSL context
		//$context = stream_context_create();
                $context = stream_context_create(array(
                    'ssl' => array(
                        'verify_peer'      => false,
                        'verify_peer_name' => false,
                        ),
                    )
                );
                
		stream_context_set_option($context, 'ssl', 'local_cert', $params['Certificate']);
	}
try {
	# Create EPP client
	$client = new Net_EPP_Client();
	# Connect
	$res = $client->connect($params['Server'], $params['Port'], 60, $use_ssl, $context);
	
}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}

	# Perform login
	$request = $client->request($xml = '
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0">
	<command>
		<login>
			<clID>'.$params['Username'].'</clID>
			<pw>'.$params['Password'].'</pw>
			<options>
			<version>1.0</version>
			<lang>en</lang>
			</options>
			<svcs>
				<objURI>urn:ietf:params:xml:ns:domain-1.0</objURI>
				<objURI>urn:ietf:params:xml:ns:contact-1.0</objURI>
			</svcs>
		</login>
	</command>
</epp>
');
	
$doc = new DOMDocument();
	$doc->preserveWhiteSpace = false;
	$doc->loadXML($request);
file_put_contents(dirname(__FILE__) .'/debug/login-request.xml', $xml, FILE_APPEND);	
file_put_contents(dirname(__FILE__) .'/debug/login-response.xml', $doc->saveXML(), FILE_APPEND);

	return $client;
}
?>
